// confirmação de ações
$(document).ready(function(){
    $('.confirm-action').click(function(e){
        // previne a ação padrão
        e.preventDefault();

        // limpa o conteúdo da modal
        $('#confirm-modal .confirm-message').empty();
        $('#confirm-modal .btn-confirm').attr('href', '#');

        // verifica a existência do atributo 'href'
        if($(this).attr('href') !== undefined){
            // caso positivo define que o link será o valor de tal atributo
            var url = $(this).attr('href');
        }
        else{
            // caso contrário define que o link será o valor do atributo 'data-href'
            var url = $(this).attr('data-href');
        }

        // define a mensagem a ser exibida ao usuário
        var mensagem = $(this).attr('data-message');

        // insere o conteúdo na modal
        $('#confirm-modal .confirm-message').html(mensagem);
        $('#confirm-modal .btn-confirm').attr('href', url);

        // exibe a modal
        $('#confirm-modal').modal('show');
    });
});






// adicionar estado de "carregando" à botões de submissão de formulários
$(document).ready(function (){
    $('form:not(.validar-formulario)').submit(function (){
        var btn = $(this).find('button[type="submit"]');
        btn.attr('data-loading-text', '<i class="fa fa-spinner fa-spin fa-lg"></i> Wait...');
        btn.button('loading');
    });
});