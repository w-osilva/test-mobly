<?php

namespace Mobly\PurchaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ItensType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt', 'datetime', array(
                'label' => 'Created At',
                'required' => false,
                'attr' => array(
                    'class' => 'datetime form-control'
                ),
                'widget' => 'single_text',
                'date_format' => \IntlDateFormatter::MEDIUM
            ))
            ->add('updatedAt', 'datetime', array(
                'label' => 'Updated At',
                'required' => false,
                'attr' => array(
                    'class' => 'datetime form-control'
                ),
                'widget' => 'single_text',
                'date_format' => \IntlDateFormatter::MEDIUM
            ))
            ->add('cost', 'text', array(
                'label' => 'Total Cost',
                'required' => 'false',
                'attr' => array(
                    'class' => 'money form-control',
                    'required' => 'false',
                )
            ))
            ->add('discount', 'text', array(
                'label' => 'Discount',
                'required' => 'false',
                'attr' => array(
                    'class' => 'money form-control',
                    'required' => 'false',
                )
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mobly\PurchaseBundle\Entity\Itens'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_itens';
    }
}
