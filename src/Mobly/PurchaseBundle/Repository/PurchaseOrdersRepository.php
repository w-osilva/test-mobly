<?php

namespace Mobly\PurchaseBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class PurchaseOrdersRepository
 * @package Mobly\PurchaseBundle\Repository
 */
class PurchaseOrdersRepository extends EntityRepository
{

    /**
     * @param array $where
     * @return array
     */
    public function findAllToXml(array $where = null){
        
        $qb = $this->createQueryBuilder('PO');
         
        if($where){
            foreach($where as $key => $value){
                $qb->andWhere("T.".$key." :".$key);
                $qb->setParameter($key, '%'.$value.'%');
             }
        }

        $query =  $qb->getQuery();
        $result = $query->getResult();

        $return = array();

        foreach($result as $order){

            $return = array(
                'order' => array(
                    '@attributes' => array(
                        'order_number' => $order->getOrderNumber()
                    ),
                    'created_at' => $order->getCreatedAt()->format('Y-m-d H:i:s'),
                    'updated_at' => $order->getUpdatedAt()->format('Y-m-d H:i:s'),
                    'cost'       => $order->getTotalCost(),
                    'discount'   => $order->getTotalDiscount(),
                    'items'

                )
            );

//            $return['order order_number="'.$order->getOrderNumber().'"']['created_at']  = $order->getCreatedAt()->format('Y-m-d H:i:s');
//            $return['order order_number="'.$order->getOrderNumber().'"']['updates_at']  = $order->getUpdatedAt()->format('Y-m-d H:i:s');
//            $return['order order_number="'.$order->getOrderNumber().'"']['cost']        = $order->getTotalCost();
//            $return['order order_number="'.$order->getOrderNumber().'"']['discount']    = $order->getTotalDiscount();
//
//            foreach($order->getItens() as $item){
//                $return['order order_number="'.$order->getOrderNumber().'"']['items']['item item_number="'.$item->getItemNumber().'"']['created_at'] = $item->getCreatedAt()->format('Y-m-d H:i:s');
//                $return['order order_number="'.$order->getOrderNumber().'"']['items']['item item_number="'.$item->getItemNumber().'"']['updates_at'] = $item->getUpdatedAt()->format('Y-m-d H:i:s');
//                $return['order order_number="'.$order->getOrderNumber().'"']['items']['item item_number="'.$item->getItemNumber().'"']['cost'] = $item->getCost();
//                $return['order order_number="'.$order->getOrderNumber().'"']['items']['item item_number="'.$item->getItemNumber().'"']['discount'] = $item->getDiscount();
//            }
        }
        
        return $return;
    }

}