<?php

namespace Mobly\PurchaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mobly\PurchaseBundle\Entity\PurchaseOrders;
use Mobly\PurchaseBundle\Form\PurchaseOrdersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Mobly\PurchaseBundle\Library\Array2XML;

/**
 * Class PurchaseOrderController
 * @package Mobly\PurchaseBundle\Controller
 */
class PurchaseOrderController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $orders = $this->getDoctrine()->getRepository('PurchaseBundle:PurchaseOrders')->findAll();

        return $this->render('PurchaseBundle:PurchaseOrders:index.html.twig', array(
            'purchaseOrders' => $orders,
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        $request = $this->getRequest();
        $order = new PurchaseOrders();
        $form = $this->createForm(new PurchaseOrdersType(), $order);
        
        $form->handleRequest($request);

        if($form->isValid()){

            $this->getDoctrine()->getConnection()->beginTransaction();
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($order);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Created successfully.');

                return $this->redirect($this->generateUrl('purchase_index'));

            } catch (\Exception $e) {
                $this->getDoctrine()->getConnection()->rollback();
            }
        }

        return $this->render('PurchaseBundle:PurchaseOrders:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction($id)
    {
        /** @var PurchaseOrders $order */
        $order   = $this->getDoctrine()->getRepository('PurchaseBundle:PurchaseOrders')->find($id);
        $request = $this->getRequest();
        $form    = $this->createForm(new PurchaseOrdersType(), $order);

        if ($request->isMethod('POST')) {
            $this->getDoctrine()->getConnection()->beginTransaction();
            $em = $this->getDoctrine()->getManager();
            try {
                foreach ($order->getItens() as $item) {
                    $em->remove($item);
                }
                $em->flush();

                $form->handleRequest($request);

                if ($form->isValid()) {

                    $em->persist($order);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add('success', 'Updated successfully.');

                    $this->getDoctrine()->getConnection()->commit();

                    return $this->redirect($this->generateUrl('purchase_index'));
                }
            } catch (\Exception $e) {
                $this->getDoctrine()->getConnection()->rollback();
            }
        }
        return $this->render('PurchaseBundle:PurchaseOrders:form.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $order = $this->getDoctrine()->getRepository('PurchaseBundle:PurchaseOrders')->find($id);
        $form = $this->createForm(new PurchaseOrdersType(), $order);

        return $this->render('PurchaseBundle:PurchaseOrders:show.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $order = $this->getDoctrine()->getRepository('PurchaseBundle:PurchaseOrders')->find($id);

        $this->getDoctrine()->getConnection()->beginTransaction();
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($order);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Deleted successfully.');

            return $this->redirect($this->generateUrl('purchase_index'));

        } catch (\Exception $e) {
            $this->getDoctrine()->getConnection()->rollback();
        }
    }


    /**
     * @return Response
     */
    public function listXmlAction(){

        $orders = $this->getDoctrine()->getRepository('PurchaseBundle:PurchaseOrders')->findAllToXml();
//        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
//        $xml =  $serializer->serialize($orders, 'xml');

        $xml = Array2XML::createXML('orders', $orders);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=utf-8');
        $response->setContent(  $xml->saveXML() );

        return $response;
    }
}
