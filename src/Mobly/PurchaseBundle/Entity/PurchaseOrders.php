<?php

namespace Mobly\PurchaseBundle\Entity;

/**
 * PurchaseOrders
 */
class PurchaseOrders
{
    /**
     * @var integer
     */
    private $orderNumber;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $totalCost;

    /**
     * @var string
     */
    private $totalDiscount;


    /**
     * Get orderNumber
     *
     * @return integer 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PurchaseOrders
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PurchaseOrders
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set totalCost
     *
     * @param string $totalCost
     * @return PurchaseOrders
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return string 
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set totalDiscount
     *
     * @param string $totalDiscount
     * @return PurchaseOrders
     */
    public function setTotalDiscount($totalDiscount)
    {
        $this->totalDiscount = $totalDiscount;

        return $this;
    }

    /**
     * Get totalDiscoun
     *
     * @return string 
     */
    public function getTotalDiscount()
    {
        return $this->totalDiscount;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $itens;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itens = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add itens
     *
     * @param \Mobly\PurchaseBundle\Entity\Itens $itens
     * @return PurchaseOrders
     */
    public function addIten(\Mobly\PurchaseBundle\Entity\Itens $itens)
    {
        $itens->setPurchaseOrder($this); // Necessário para gravação do formulário em cascade
        $this->itens[] = $itens;

        return $this;
    }

    /**
     * Remove itens
     *
     * @param \Mobly\PurchaseBundle\Entity\Itens $itens
     */
    public function removeIten(\Mobly\PurchaseBundle\Entity\Itens $itens)
    {
        $this->itens->removeElement($itens);
    }

    /**
     * Get itens
     *
     * @return \Doctrine\Common\Collections\Collection|Itens[]
     */
    public function getItens()
    {
        return $this->itens;
    }

    public function preInsertValues()
    {
        $this->setCreatedAt(new \Datetime());
        $this->setUpdatedAt(new \Datetime());
        $this->sumTotalCost();
        $this->sumTotalDiscount();
    }

    public function preUpdateValues()
    {
        $this->setUpdatedAt(new \Datetime());
        $this->sumTotalCost();
        $this->sumTotalDiscount();
    }

    public function sumTotalCost()
    {
        $this->totalCost = 0;
        foreach($this->getItens() as $item){
            $this->totalCost += (float)$item->getCost();
        }
    }

    public function sumTotalDiscount()
    {
        $this->totalDiscount = 0;
        foreach($this->getItens() as $item){
            $this->totalDiscount += (float)$item->getDiscount();
        }
    }
}

