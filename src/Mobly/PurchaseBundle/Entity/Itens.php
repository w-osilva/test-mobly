<?php

namespace Mobly\PurchaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Itens
 */
class Itens
{
    /**
     * @var integer
     */
    private $itemNumber;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $cost;

    /**
     * @var string
     */
    private $discount;

    /**
     * @var \Mobly\PurchaseBundle\Entity\PurchaseOrders
     */
    private $purchaseOrder;

    /**
     * Get itemNumber
     *
     * @return integer 
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Itens
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Itens
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set cost
     *
     * @param string $cost
     * @return Itens
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return Itens
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set purchaseOrder
     *
     * @param \Mobly\PurchaseBundle\Entity\PurchaseOrders $purchaseOrder
     * @return Itens
     */
    public function setPurchaseOrder(\Mobly\PurchaseBundle\Entity\PurchaseOrders $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;
        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \Mobly\PurchaseBundle\Entity\PurchaseOrders 
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    public function preInsertValues()
    {
        $this->setCreatedAt(new \Datetime());
        $this->setUpdatedAt(new \Datetime());
        $this->getPurchaseOrder()->preInsertValues();
    }

    public function preUpdateValues()
    {
        $this->setUpdatedAt(new \Datetime());
        $this->getPurchaseOrder()->preUpdateValues();
    }
}
