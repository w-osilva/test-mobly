<?php

namespace Mobly\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Index:index.html.twig');
    }
}
