<?php

namespace Mobly\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mobly\UserBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    private $users = array(
        array(
            'username'              => 'admin',
            'username_canonical'    => 'admin',
            'email'                 => 'admin@admin.com',
            'email_canonical'       => 'admin@admin.com',
            'enabled'               =>  1,
            'password'              => 'admin',
            'roles'                 => 'ROLE_SUPER_ADMIN'
        ),

    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->$users as $user) {
            $entity = new User();

            $entity->setUsername($user['username']);
            $entity->setUsernameCanonical($user['username_canonical']);
            $entity->setEmail($user['email']);
            $entity->setEmailCanonical($user['email_canonical']);
            $entity->setEnabled($user['enabled']);
            $entity->setPassword($user['password']);
            $entity->setRoles($user['roles']);

            $manager->persist($entity);
            $manager->flush();
        }
    }
}