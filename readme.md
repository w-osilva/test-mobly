O Teste consiste em:

Criar um CRUD de purchase_order + item:

A entidade purchase_order consiste nos seguintes campos obrigatórios:

- order_number - 10 caracteres, único
- created_at
- updated_at
- total_cost - decimal 10,2
- total_discount - decimal 10,2

A entidade item consiste nos seguintes campos obrigatórios:

- item_number - 3 caracteres, único por pedido
- created_at
- updated_at
- cost - decimal 10,2
- discount - decimal 10,2
- purchase_order_id

O software precisa ser escrito em PHP 5.4 e não necessariamente em algum framework, mas preferencialmente em Symfony (última versão).

O banco de dados será o MySQL.

Precisa ter uma area restrita para o CRUD. Os usuários podem estar no banco de dados.

Precisa funcionar em LINUX.

Utilizar o Twitter Bootstrap (http://getbootstrap.com/).

Extra:

1 - Ler o XML anexo a partir de uma URL e persistir no banco de dados;

2 - Disponibilizar um webservice onde a resposta será a estrutura do XML anexo com os dados salvos no seu banco de dados;


INSTRUÇÔES PARA INSTALAÇÃO

Fazer o clone do projeto

1- Atualizar o Composer

php composer update

2- Configurar parameters.yml

Criar o banco de dados 'mobly-test' no MySQL

Criar usuario com permissão de acesso 'Local'

Configurar parameters (database_user, database_password)

3- Gerar schema da database

php app/console doctrine:schema:create

php app/console doctrine:schema:update --force

4- Fazer migrations

php app/console doctrine:migrations:diff

php app/console doctrine:migrations:migrate

//TODO: Criar carga de dados (fixtures) para usuarios default na aplicação, configurar JMSSerializeBundle, criar url para visualizar xml (webservice)
5- Carga de dados com fixtures
php app/console doctrine:fixtures:load